const inputs = document.querySelectorAll(".input");
const userInput = document.getElementById("user");
const pwInput = document.getElementById("pass");
const logIn = document.querySelector('.btn');


function addcl(){
	let parent = this.parentNode.parentNode;
	parent.classList.add("focus");
}
// capture password
function remcl(){
	let parent = this.parentNode.parentNode;
	if(this.value == ""){
		parent.classList.remove("focus");
	}
}


inputs.forEach(input => {
	input.addEventListener("focus", addcl);
	input.addEventListener("blur", remcl);
});

//Login 
logIn.addEventListener('click', function(){
	const  Uname = userInput.value;
	const  Pword = pwInput.value;
	//validation if password or email is empty
	if(Uname === "" || Pword === ""){
		alert("Username or Password cannot be blank.");
		return;
	}
	// wrong user or password
	else if (Uname !== "admin" || Pword !== "adminadmin") {
		alert("Username or Password is incorrect");
		return;
	}
	//validation if password length is less than 8
	else if(Pword.length < 8){
		alert("Password length must be 8 or longer");
		return;
	}
	//validation for admin login
	else if(Uname === "admin" && Pword === "adminadmin"){
		// alert("Admin login successful");
		document.getElementById("myForm").action = "RentSheet/Rindex.html";
		return;
	}
});