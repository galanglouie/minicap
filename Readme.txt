ABOUT

A website app for bike rental services,
(ex. Barangay Bike Rental Services to provide transpo for the people of their barangay.)
in this pandemic, most of us, are having difficulty
in using public transportation. With this app, it can help the barangay
to monitor their bike availability and keep track with 
their revenues as well. 

To use the app:
Login as admin
Username:admin
Password:adminadmin

How to use the app:

1.Choose the type of bike you want to rent.
2.Applicable rates for specific type will show
3.Enter customer name and no. of hours of rent.
4.Click in log, the data will show on rental sheet.
5.After the payment is done, you can click on paid;
6.then, the successful transaction will appear on the revenue sheet.

For adding bike inventory:

1.Type in the number of bike you want to add,then
2.Click on add bike button, the additional number of bike will be added to available bike

For removing of bike inventory: (for repair or damage)

1.Type in the number of bike you want to remove, then
2. Click on remove bike button, the number of bike will be deducted from available bike.

