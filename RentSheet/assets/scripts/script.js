let rate = 0;

const customers = [];

const paid = [];

const revenue = [];

// hardcoded since wala naman tayo admin panel for adding editing ng number of bikes
let bikes = 20;
//capture button and store it to sumBtn	
const sumBtn = document.getElementById('sumBtn');

// capture button and store it to const addBtn
const addBtn = document.getElementById('customerRent').firstElementChild.nextElementSibling.nextElementSibling;

document.getElementById('available').textContent = bikes;

// capture input and store it to const inventoryInput 	
const inventoryInput = document.getElementById('inventory');

//capture addbike button
const addBike = document.getElementById('addInv');
// capture remove bike button
const minusBike = document.getElementById('minusInv');

// capture the input rate
const rateInput = document.getElementById('rate');

// add event listener for addBike
addBike.addEventListener('click',function(){
	inventory = parseFloat(inventoryInput.value);		
	if(inventory < 1){											
		alert("Additional bike cannot be less than 1 item.");	
		return;													
	}else if (inventoryInput.value === "") {
		alert("Please input a number");
		return;	
	}else {
		document.getElementById('available').textContent = bikes += inventory;
		inventoryInput.value ="";
		return;
	}											
});

// add event listener for minusBike
minusBike.addEventListener('click',function(){
	inventory = parseFloat(inventoryInput.value);
	if(inventory > bikes){											
		alert("You're trying to remove more than your bikes number");	
		return;													
	}else if (inventory < 0 ){
		alert("You're trying to input a negative value");	
		return;	
	}
	else if (inventoryInput.value === "") {
		alert("Please input a number");
		return;	
	}else {
		document.getElementById('available').textContent = bikes -= inventory;
		inventoryInput.value ="";	
		return;
	}								
});

// capture select 
function typeOfBike() {
	let type = document.getElementById("Select1");
	document.getElementById("type").textContent = " : " + type.value;

	//Give rates per type selection
	if(type.value === 'Bike'){
		document.getElementById('rate').value = 5;
	}
	if(type.value === 'Mountain Bike'){
		document.getElementById('rate').value = 10;
	}
	if(type.value === 'Electric Bike'){
		document.getElementById('rate').value = 15;
	}
	if(type.value === "Pick a type"){
		document.getElementById('rate').value = "Choose type to get hourly rates";
	}
	if(type.value === ""){
		document.getElementById('rate').value = "Choose type to get hourly rates";
	}
}



// function for adding data to customer rental sheet
function postCustomer(){

	customers.forEach(function(indivCustomer, index){
		const newRow = document.createElement('tr');

		newRow.innerHTML = `<td>${indivCustomer.customerName}</td>
							<td>${indivCustomer.timeStamp}</td>
							<td>${indivCustomer.noOfHours}</td>
							<td>${indivCustomer.rentRate}</td>
							<td><button class="btn btn-danger removeBtn" data-id="${index}">Remove</button>
							<td><button class="btn paidBtn" data-id="${index}">Paid</button></td>`;
		document.getElementById('rentDetails').appendChild(newRow);
	});
}

function postPaidCustomer(){
	paid.forEach(function(indivCustomer, index){
		const newRow = document.createElement('tr');

		newRow.innerHTML = `<td>${indivCustomer.paidName}</td>
							<td>${indivCustomer.paidTime}</td>
							<td>${indivCustomer.paidHour}</td>
							<td>${indivCustomer.paidTotal}</td>`;
		document.getElementById('paidDetails').appendChild(newRow);
	});
}

function showTime() {
 var date = new Date().toLocaleTimeString();
 	return date;
}

// add event listener for addBtn
addBtn.addEventListener('click',function(){
	rate = parseFloat(rateInput.value);
	const customerName = addBtn.parentElement.firstElementChild.value;
	const noOfHours = addBtn.previousElementSibling.value;
	const timeStamp = showTime();
	const rentRate = rate * noOfHours;

	let type = document.getElementById("Select1");
	document.getElementById("type").textContent = " : " + type.value;

	if(noOfHours < 1){												
		alert("Rental hour(s) cannot be less than 1 hour.");		
		return;														
	}else if (rate === 0) {
		alert("Please enter a rate");
		return;
	}else if (customerName === "") {
		alert("Please enter a name");		
		return;	
	}else if (bikes <= 0){
		alert("There are no more bikes available");
		return;
	}else if (type.value === ""){
		alert("You did not pick the type of bike!");
		return;
	}else {
		document.getElementById('rentDetails').innerHTML = "";
		const customer = {
			customerName,
			timeStamp,
			noOfHours,
			rentRate
		}
		bikes--;
		document.getElementById('available').textContent = bikes;
		customers.push(customer);
		postCustomer();
		console.log(rate);
	}
	
});

// sumBtn.addEventListener('click',function(){
// 	document.getElementById('sum').textContent = revenue.()
// });

// create event listener for remove and paid button
document.addEventListener('click',function(event){

	if (event.target.classList.contains('removeBtn')) {
		document.getElementById('rentDetails').innerHTML = "";
		const index = event.target.getAttribute('data-id');
		customers.splice(index, 1);
		postCustomer();
		bikes++;
		document.getElementById('available').textContent = bikes;

	}if (event.target.classList.contains('paidBtn')) {
		document.getElementById('paidDetails').innerHTML = "";
		const index = event.target.getAttribute('data-id');
		const paidName = event.target.parentElement.parentElement.firstElementChild.innerHTML;
		const paidTime = event.target.parentElement.parentElement.firstElementChild.nextElementSibling.innerHTML;
		const paidHour = event.target.parentElement.parentElement.firstElementChild.nextElementSibling.nextElementSibling.innerHTML;
		const paidTotal = parseFloat(event.target.parentElement.parentElement.firstElementChild.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML);
		const paidCustomer = {
			paidName,
			paidTime,
			paidHour,
			paidTotal 
		}
		revenue.push(paidTotal);
		paid.push(paidCustomer);
		postPaidCustomer();
		document.getElementById('rentDetails').innerHTML = "";
		customers.splice(index, 1);
		postCustomer();
		bikes++;
		document.getElementById('available').textContent = bikes;
	}

});

// create event listener for sum button
sumBtn.addEventListener('click',function(){											
  document.getElementById('sum').textContent = revenue.reduce(function(a,b){		
    return a + b
  }, 0);
});


//UI styling Code
document.querySelector('.b').style.color = "green";

//logout button
function logoutBtn() {
  location.href = "../index.html";
}













// UI Alert for replacing boring alert window
// static showAlert(message, className) {
//     const div = document.createElement('div');
//     div.className = `alert alert-${className}`;
//     div.appendChild(document.createTextNode(message));
//     const container = document.querySelector('.container');
//     const form = document.querySelector('#book-form');
//     container.insertBefore(div, form);

//     // Vanish in 3 seconds
//     setTimeout(() => document.querySelector('.alert').remove(), 3000);
//   }